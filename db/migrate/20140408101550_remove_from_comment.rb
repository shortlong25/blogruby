class RemoveFromComment < ActiveRecord::Migration
  def change
	remove_column :comments, :body, :string
	remove_column :comments, :text, :string
  end
end
